#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  7 11:57:25 2024

@author: jeremieritoux
"""

import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import re

def Read_Synchrotron_File(filename,calib_file,ROI_file):

    def Load_File(filename):
        h5 = h5py.File(filename, "r").get('scan_data').get("data")
        Map = np.array(h5)
        Map=np.nan_to_num(Map)
        Map[Map>1e6]=0
        
        return Map
    
    def Read_ABC(calib_file):
        
        file = open(calib_file, "r")
        content = file.read()
        
        pattern = r'A = ([-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)\nB = ([-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)\nC = ([-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)'
    
        # Find A, B, and C values using regular expression
        matches = re.findall(pattern, content)
        
        # Extracting values
        if matches:
            A = float(matches[0][0])
            B = float(matches[0][1])
            C = float(matches[0][2])
            print("A =", A)
            print("B =", B)
            print("C =", C)
        else:
            print("A, B, and C values not found.")
        
        return A,B
            
    def Read_ROI_file(ROI_file):
        
        def sort_ROI_energies_and_names(ROI_energies, ROI_names):
            # Convert ROI_energies to numpy array for easier sorting
            ROI_energies = np.array(ROI_energies)
        
            # Sort ROI_energies and ROI_names based on the beginning energy values
            sorted_indices = np.argsort(ROI_energies[:, 0])
            ROI_energies_sorted = ROI_energies[sorted_indices]
            ROI_names_sorted = np.array(ROI_names)[sorted_indices]
        
            return ROI_energies_sorted, ROI_names_sorted
        
        with open(ROI_file, "r") as file:
            data = file.read()
        
        # Extract ROI data using regular expressions
        roi_pattern = re.compile(r'\[ROI\.roidict\.(.*?)\]\ntype = .*?\nfrom = (.*?)\nto = (.*?)\n', re.DOTALL)
        rois = roi_pattern.findall(data)
        
        # Extract ROI names, excluding the first ROI
        ROI_names = [roi[0] for roi in rois[1:]]
        
        # Create a 2D array, excluding the first ROI
        nbROI = len(rois) - 1
        ROI_energies = [[None, None] for _ in range(nbROI)]
        
        # Fill the array with "from" and "to" values for each ROI, excluding the first ROI
        for i, (_, fr, to) in enumerate(rois[1:]):
            ROI_energies[i][0] = float(fr)
            ROI_energies[i][1] = float(to)
        
        ROI_energies=np.array(ROI_energies)

        ROI_energies_sorted, ROI_names_sorted=sort_ROI_energies_and_names(ROI_energies, ROI_names)
        
        return ROI_energies_sorted, ROI_names_sorted
    
    def ROI_Signal(Map,ROI_energies,ROI_names,A,B):
        
        Sig=np.sum(Map,axis=(0,1))
        
        # Calculate energy values
        Energy = A + B * np.arange(2048)
    
        fig,ax=plt.subplots(1)
        ax.plot(Energy, Sig, color='blue', label='Spectrum Signal')
        ax.set_xlabel('Energy')
        ax.set_ylabel('Intensity')
        fig.suptitle('Spectrum Signal with ROI')
    
        # Fill regions of interest
        for i, (roi, (fr, to)) in enumerate(zip(ROI_names, ROI_energies)):
            ax.fill_between(Energy, 0, Sig, where=((Energy >= fr) & (Energy <= to)), alpha=0.3, label=f'{roi}')
            
        ax.set_yscale("log")
        ax.legend()
        
    def calculate_ROI_sum(Map, ROI_energies, A, B):
        Nx, Ny, _ = Map.shape
        Nb_ROI = len(ROI_energies)
        ROI_sum = np.zeros((Nx, Ny, Nb_ROI))
    
        for k, (fr, to) in enumerate(ROI_energies):
            # Find indices corresponding to ROI energy range
            start_idx = int((fr - A) / B)
            end_idx = int((to - A) / B)
            
    
            # Calculate ROI sum for each voxel
            for i in range(Nx):
                for j in range(Ny):
                    ROI_sum[i, j, k] = np.sum(Map[i, j, start_idx:end_idx+1])
    
        return ROI_sum
    
    Map=Load_File(filename)
    
    A,B=Read_ABC(calib_file)
    ROI_energies,ROI_names=Read_ROI_file(ROI_file)
        
    for i in range(1,len(ROI_energies)-1):
        mean_value1=(ROI_energies[i-1,1]+ROI_energies[i,0])/2
        mean_value2=(ROI_energies[i,1]+ROI_energies[i+1,0])/2
        
        ROI_energies[i-1,1]=mean_value1
        ROI_energies[i,0]=mean_value1
        
        ROI_energies[i,1]=mean_value2
        ROI_energies[i+1,0]=mean_value2
    
    
    ROI_energies_mean = np.mean(ROI_energies, axis=1)
    
    #ROI_Signal(Map,ROI_energies,ROI_names,A,B)
    
    ROI_Map=calculate_ROI_sum(Map, ROI_energies, A, B)
    
    return ROI_Map,ROI_energies_mean,ROI_energies,ROI_names


filename="/Volumes/Transcend2To/Synchrotron 2023/PUMA_2023_06/CORR_I0_DTIME/Crev-gauche_LAK-PB2_60um-0044_corr.h5"
calib_file="/Volumes/Transcend2To/Synchrotron 2023/PUMA_2023_06/cfg_pymca/calib_for_ROI.calib"
ROI_file="/Volumes/Transcend2To/Synchrotron 2023/PUMA_2023_06/ROI_ini/ROI_STW2022.ini"

ROI_Map,ROI_energies_mean,ROI_energies,ROI_names=Read_Synchrotron_File(filename,calib_file,ROI_file)

#plt.imshow(ROI_Map[:,:,6])
