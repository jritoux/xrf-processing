#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 10:21:47 2024

@author: jeremieritoux
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from sklearn.cluster import KMeans
from scipy.optimize import nnls
from PIL import Image
from ipywidgets import interactive, FloatSlider
from IPython.display import display
from matplotlib.widgets import Slider
from scipy.ndimage import binary_dilation, binary_erosion
from sklearn.decomposition import PCA
from scipy.optimize import curve_fit
from scipy.interpolate import CubicSpline
from PIL import Image, ImageDraw,ImageFont



mpl.rcParams["font.family"] = "Times New Roman"
mpl.rcParams['font.size'] = 6
mpl.rcParams["axes.xmargin"]=0
mpl.rcParams['axes.titlesize'] = 12
mpl.rcParams['axes.titleweight'] = "bold"
mpl.rcParams['font.weight'] = "bold"
mpl.rcParams['axes.labelsize'] = 10
mpl.rcParams['xtick.labelsize'] = 5
mpl.rcParams['ytick.labelsize'] = 5

#cluster_names=["LeastFe","Fossil","Sediment","Spot"]
#cluster_color=["red","deeppink","yellowgreen","coral"]

cluster_names=["Fossil","Sediment","Spot"]
cluster_color=["deeppink","yellowgreen","coral"]

#Pour enregistrer un array 2D comme image png
def Save_Data(array,filename):
    img = Image.fromarray(np.array(255*array/np.max(array), dtype=np.uint8))
    img.save(filename)

#Pour lire le fichier .npz correspondant au bon échantillon, et retourner l'array 3D
#+ Ini_Compo qui correspond à un point sur le fossile et un point sur le sédiment pour initialiser K-means
def LoadData(Sample_Name):

    if Sample_Name=="LAKPB2B8":
        filename='/Users/jeremieritoux/Documents/THESE/ANALYSES/XRF Processing/data_BLOB_B8.npz'
    if Sample_Name=="LAKPB2B9":
        filename='/Users/jeremieritoux/Documents/THESE/ANALYSES/XRF Processing/data_BLOB_B9.npz'
    if Sample_Name=="LAKPB2D1a":
        filename='/Users/jeremieritoux/Documents/THESE/ANALYSES/XRF Processing/data_BLOB_D1a.npz'
    if Sample_Name=="LAKPB2D1b":
        filename='/Users/jeremieritoux/Documents/THESE/ANALYSES/XRF Processing/LAK_PB2_D1b.npz'
    
    data=np.load(filename)
    Elements_Map = data['Elements_Map']
    Elements_Names = data['Elements_Names']
    
    if Sample_Name=="LAKPB2B8":
        Ini_Compo=np.array([Elements_Map[250,1320,:],Elements_Map[600,540,:]])
    if Sample_Name=="LAKPB2B9":
        Ini_Compo=np.array([Elements_Map[400,2100,:],Elements_Map[770,1950,:],Elements_Map[105,2590,:]])
    if Sample_Name=="LAKPB2D1a":
        Ini_Compo=np.array([Elements_Map[210,752 ,:],Elements_Map[ 220,130,:]])
    if Sample_Name=="LAKPB2D1b":
        Ini_Compo=np.array([Elements_Map[100,730,:],Elements_Map[ 65,490,:]])
    
    nx,ny,nbElements=np.shape(Elements_Map)
    
    def MaskData(mask_image_path):

        mask_image = Image.open(mask_image_path)
        mask_array = np.array(mask_image)
        MASK = (mask_array > 0)
        
        return MASK
    
    MASK=None
    
    if Sample_Name == "LAKPB2B8":
        MASK = MaskData('/Users/jeremieritoux/Documents/THESE/ANALYSES/XRF Processing/LAKPB2B8_Mask.png')
        # Check if Elements_Map is numeric, if not convert it to numeric
        if Elements_Map.dtype.kind == 'U':
            Elements_Map = Elements_Map.astype(float)  # Convert string data to float
        Elements_Map = Elements_Map * MASK[:, :, np.newaxis]

    
    return Elements_Map,Elements_Names,Ini_Compo,nx,ny,nbElements,MASK

#Pour faire la differentiation fossile et sédiment. Retourne un array 2D ClusterMap =0 si fossile, 1 si sédiment
def Kmean_Clustering(Elements_Map, initial_centers, Show_Clusters=True):
    reshaped_data = Elements_Map.reshape((-1, Elements_Map.shape[-1]))
    kmeans = KMeans(n_clusters=len(initial_centers), init=initial_centers, random_state=42)
    kmeans.fit(reshaped_data)
    ClusterMap = kmeans.labels_.reshape(Elements_Map.shape[:-1])
    cluster_centers = kmeans.cluster_centers_
    
    if Show_Clusters:
        
        fig,ax=plt.subplots(2)
        
        nbCluster = len(initial_centers)
        cmap_cluster = mpl.colors.ListedColormap(cluster_color[:nbCluster])
        ax[0].imshow(ClusterMap, cmap=cmap_cluster)
        ax[0].set_title("Preliminary Clustering")

        bar_width = 0.35  # Adjust the width of the bars
        x = np.arange(len(Elements_Names))
        
        for c in range(2):
            ax[1].bar(x + c * bar_width, cluster_centers[c,:], width=bar_width, label=cluster_names[c], color=cluster_color[c])
        ax[1].set_yscale("log")
        ax[1].set_title("Cluster Centers")
        ax[1].set_xticks(x + bar_width * (nbCluster - 1) / 2, Elements_Names)
        
    return ClusterMap, cluster_centers, len(initial_centers)

#Pour generer l'histogramme correspondant à l'array data
def Histo(data,max_bin):
    min_bin=1
    num_bins=int(max_bin-1)
    bin_edges = np.linspace(min_bin, max_bin, num_bins +1)
    histogram, _ = np.histogram(data, bins=bin_edges)  
    return bin_edges,histogram

#Pour representer la cartographie de chaque élément
def Plot_Elements(Map, Sample_Name):
    fig, ax = plt.subplots(7, 4)
    fig.suptitle("Cartographies par éléments - "+Sample_Name, fontsize=30)
        
    for i in range(7):
        for j in range(4):
            index_element = j * 7 + i
            ax[i, j].imshow(Map[:, :, index_element])
            ax[i, j].set_ylabel(Elements_Names[index_element])

#Pour representer la cortographie des résidus pour chaque élément (Positif et Negatif)                               
def Plot_Residus(Map, Sample_Name, Save=False):
    fig, ax = plt.subplots(7, 4)
    fig.suptitle(Sample_Name + " - Résidu Positif", fontsize=30)
    
    Map_Pos = np.log(1 + np.where(Map > 0, Map, 0))
    
    for i in range(7):
        for j in range(4):
            index_element = j * 7 + i
            ax[i, j].imshow(Map_Pos[:, :, index_element], cmap='gray')
            ax[i, j].set_ylabel(Elements_Names[index_element])
            if Save and Elements_Names[index_element][:2]!="UK":
                pos_img = Image.fromarray((Map_Pos[:, :, index_element] * 255).astype(np.uint8))
                pos_img.save(Sample_Name + " - Positif_" + Elements_Names[index_element] + ".png")


    fig, ax = plt.subplots(7, 4)
    fig.suptitle(Sample_Name + " - Résidu Négatif", fontsize=30)
    
    Map_Neg = -Map
    Map_Neg = np.log(1 + np.where(Map_Neg > 0, Map_Neg, 0))
    
    for i in range(7):
        for j in range(4):
            index_element = j * 7 + i
            ax[i, j].imshow(Map_Neg[:, :, index_element], cmap='gray')
            ax[i, j].set_ylabel(Elements_Names[index_element])
            if Save and Elements_Names[index_element][:2]!="UK":
                neg_img = Image.fromarray((Map_Neg[:, :, index_element] * 255).astype(np.uint8))
                neg_img.save(Sample_Name + " - Négatif_" + Elements_Names[index_element] + ".png")

#Pour visualiser cartographies par éléments + les histogrammes associés au fossile et au sédiments
def Plot_Elements_and_Histo(Map,ClusterMap):
    
    fig,ax1=plt.subplots(7,4)
    fig,ax2=plt.subplots(7,4)
    
    for i in range(7):
        for j in range(2):
            index_element=j*7+i
            ax1[i,j*2].imshow(1+Map[:,:,index_element],norm=mpl.colors.LogNorm())
            ax1[i,j*2].set_ylabel(Elements_Names[index_element])
            
            
            max_bin=int(np.max(Map[:,:,index_element]))
            Hist=np.zeros([max_bin-1,nbCluster])
            
            for c in range(nbCluster):
                Elements_Map_Clust=Map[:,:,index_element].copy()
                Elements_Map_Clust[ClusterMap != c] = -1
                bin_edges,histogram=Histo(Elements_Map_Clust.reshape(-1),max_bin)
                Hist[:,c]=histogram
            
            
            
            for c in range(nbCluster):
                ax1[i,j*2+1].plot(bin_edges[:-1],Hist[:,c],label=cluster_names[c],color=cluster_color[c])
            
            ax1[i,j*2+1].plot(bin_edges[:-1],np.sum(Hist,axis=1),color="black")
            ax1[i,j*2+1].legend(loc='upper right',frameon=False)
            
            ################################################
    
            index_element=(j+2)*7+i
            ax2[i,j*2].imshow(1+Map[:,:,index_element],norm=mpl.colors.LogNorm())
            ax2[i,j*2].set_ylabel(Elements_Names[index_element])
            
            
            max_bin=int(np.max(Map[:,:,index_element]))
            Hist=np.zeros([max_bin-1,nbCluster])

            
            for c in range(nbCluster):
                Elements_Map_Clust=Map[:,:,index_element].copy()
                Elements_Map_Clust[ClusterMap != c] = -1
                bin_edges,histogram=Histo(Elements_Map_Clust.reshape(-1),max_bin)
                Hist[:,c]=histogram
                
            
            for c in range(nbCluster):
                ax2[i,j*2+1].plot(bin_edges[:-1],Hist[:,c],label=cluster_names[c],color=cluster_color[c])
            
            ax2[i,j*2+1].plot(bin_edges[:-1],np.sum(Hist,axis=1),color="black")
            ax2[i,j*2+1].legend(loc='upper right',frameon=False)

#Jutilise cette fonction pour avoir une meilleure approximation du maximum de l'histograme
#=> Permet de faire un interpolation quadratiques des points autour du max et prendre le max de cette interpolation
def Quadratic_Interpolation(data):
    
    max_index = np.argmax(data)
    if max_index==0:
        return 0
    
    left_value = data[max_index - 1]
    right_value = data[max_index + 1]
    
    if left_value == right_value:
        return max_index
    
    delta_x = -0.5 * (right_value - left_value) / (right_value - 2 * data[max_index] + left_value)
    
    return max_index + delta_x

#Pour trouver le max de chaque histogramme fossile et sédiment pour chaque élément
def Calc_Pure_Phases(Elements_Map,ClusterMap,Show_Composition=True):
    Pure_Phases=np.zeros([nbElements,nbCluster])
    
    for index_element in range(nbElements):
        
        max_bin=int(np.max(Elements_Map[:,:,index_element]))
        
        for c in range(0,nbCluster):
            Elements_Map_Clust=Elements_Map[:,:,index_element].copy()
            Elements_Map_Clust[ClusterMap != c] = -1
            bin_edges,histogram=Histo(Elements_Map_Clust.reshape(-1),max_bin)
            Pure_Phases[index_element,c]=Quadratic_Interpolation(histogram)
            #Pure_Phases[index_element,c]=np.dot(histogram, bin_edges[:-1]) / np.sum(histogram)
    
    if Show_Composition:
        fig,ax=plt.subplots(1)
        bar_width = 0.35  # Adjust the width of the bars
        x = np.arange(len(Elements_Names))
        
        for c in range(2):
            ax.bar(x + c * bar_width, Pure_Phases[:, c]/np.sum(Pure_Phases[:, c]), width=bar_width, label=cluster_names[c], color=cluster_color[c])

        ax.set_xticks(x + bar_width * (nbCluster - 1) / 2, Elements_Names)
        ax.set_yscale("log")
        ax.set_ylim([None,1])
        ax.legend(loc='upper right',frameon=False)
    
    return Pure_Phases

#NNLS à partir des compositions générées precedemment
def Estimate_Composition(Elements_Map, Pure_Phases,Show_Composition=True):
    
    Composition = np.zeros((nx, ny, nbCluster))
    Elements_Map_2D = Elements_Map.reshape((nx * ny, nbElements))
    
    
    for i in range(nx):
        for j in range(ny):
            Composition[i, j], _ = nnls(Pure_Phases, Elements_Map_2D[i * ny + j])
    
    if Show_Composition:
        fig,ax=plt.subplots(nbCluster+1)
        for c in range(nbCluster):
            ax[c].imshow(Composition[:,:,c])
            ax[c].set_title(cluster_names[c])
            
        ax[nbCluster].imshow(np.sum(Composition,axis=2))
        ax[nbCluster].set_title("Somme")
        
    return Composition.reshape((nx, ny, nbCluster))

#Permet de représenter le résidu pour chaque élément
def RGB_differences(List_E,sign,Save):
    
    def Save_Image(Array, title, List_E,sign):
        X = Array.copy()
        X = (X - np.min(X)) / (np.max(X) - np.min(X))
        X_uint8 = (X * 255).astype(np.uint8)
        im = Image.fromarray(X_uint8)
        
        # Get image size
        width, height = im.size
        
        Colors=["red","green","blue"]
        Loc_X=[150,100,50]
        
        
        draw = ImageDraw.Draw(im)
        circle_radius = 20
        
        for k in range(len(List_E)):
            circle_center = (width -250, height - Loc_X[k])
            draw.ellipse([(circle_center[0] - circle_radius, circle_center[1] - circle_radius),
                          (circle_center[0] + circle_radius, circle_center[1] + circle_radius)],
                         fill=Colors[k])
            
            if sign[k]==1:
                TXT="Résidu Positif - "+List_E[k]
            else:
                TXT="Résidu Negatif - "+List_E[k]
                
            draw.text((circle_center[0]+50, circle_center[1]-10), TXT, fill="white",font=ImageFont.truetype("Arial", 20))
        
        draw.text((width-150,height - 220), Sample_Name, fill="white",font=ImageFont.truetype("Arial", 24))
        
        # Save the image
        im.save(title, "png")

    Carto_RGB=np.zeros([nx,ny,3])
    
    for i in range(len(List_E)):
        e=list(Elements_Names).index(List_E[i])
        
        Pure_Phases_e=Pure_Phases[e,:].T
        Difference_e = sign[i]*(Elements_Map[:,:,e] - np.dot(Composition, Pure_Phases_e))
        
        Carto=np.log(1+np.where(Difference_e > 0, Difference_e, 0))
        Carto_RGB[:,:,i] = Carto/np.max(Carto)

    fig,ax=plt.subplots(1)
    
    color=["r","green","b"]
    for i in range(len(List_E)):
        if sign[i]==1:
            label="Résidu Positif - "+List_E[i]
        else:
            label="Résidu Positif - "+List_E[i]
            
        ax.scatter(0,0,color=color[i],label=label)

    ax.imshow(Carto_RGB)    
    ax.set_title("Résidu")
    
    legend0=ax.legend(loc="lower right",frameon=False)
    for text in legend0.get_texts():
        text.set_color('white')
       
    title=''
    for k in range(len(List_E)):
        if sign[k]==1:
            title=title+"_Pos-"+List_E[k]
        else:
            title=title+"_Neg-"+List_E[k]
        
    if Save:
        Save_Image(Carto_RGB,Sample_Name+title+".png",List_E,sign)

#J'ai créé cette fonction pour valider ma méthode de l'histogramme pour trouver phase pure.
#=> permet d'expend ou éroder la cartographie des clusters créée avec Kmean_Clustering
def Expand_Cluster(ClusterMap,Elements_Map,SelectedElements,nbExpend=10):
    
    
    def Expension(ClusterMap,expend_value):
        if expend_value<0:
            ClusterMap_Exp = binary_erosion(ClusterMap, iterations=-expend_value).astype(int)
        if expend_value==0:
            ClusterMap_Exp=ClusterMap
        if expend_value>0:
            ClusterMap_Exp = binary_dilation(ClusterMap, iterations=expend_value).astype(int)
        return ClusterMap_Exp
    
        
    ClusterSum=np.zeros([nx,ny])
    
    fig_histo,ax_histo=plt.subplots(2,len(SelectedElements)) 
    color_curve=plt.cm.rainbow(np.linspace(0,1,2*nbExpend))
    lineshape=["-",":"]

    values_to_plot=np.zeros([2,nbCluster,2*nbExpend,len(SelectedElements)])

    for expend_value in range(-nbExpend,nbExpend):
        ClusterMap_Exp=Expension(ClusterMap,expend_value)
        ClusterSum+=ClusterMap_Exp

        for i in range(len(SelectedElements)):
            e=list(Elements_Names).index(SelectedElements[i])
            for c in range(nbCluster):
                #Cluster Center
                cluster_element = Elements_Map[:,:,e][ClusterMap_Exp == c]
                mean_cluster_element = np.mean(cluster_element)
                values_to_plot[0,c,nbExpend+expend_value,i]=mean_cluster_element
                
                #Histograms
                Elements_Map_Clust=Elements_Map[:,:,e].copy()
                Elements_Map_Clust[ClusterMap_Exp != c] = -1
                bin_edges,histogram=Histo(Elements_Map_Clust.reshape(-1),np.max(Elements_Map_Clust))
                
                #values_to_plot[1,c,nbExpend+expend_value,i]=list(histogram).index(max(histogram))
                max_value=Quadratic_Interpolation(histogram)
                values_to_plot[1,c,nbExpend+expend_value,i]=max_value

                #Plot Histograms
                ax_histo[0,i].plot(bin_edges[:-1],histogram,lineshape[c],color=color_curve[nbExpend+expend_value],linewidth=0.75)
                ax_histo[0,i].scatter(max_value,histogram[int(max_value)])
        
    for i in range(len(SelectedElements)):
        e=list(Elements_Names).index(SelectedElements[i])
        bin_edges,histogram=Histo(Elements_Map[:,:,e].reshape(-1),np.max(Elements_Map[:,:,e]))
        ax_histo[0,i].plot(bin_edges[:-1],histogram,color="black")
        ax_histo[0,i].set_title("Histogrammes "+Elements_Names[e])
        ax_histo[0,i].set_xlabel("Intensité")
        ax_histo[0,i].set_ylabel("Nombre de pixels")
        
        
    for i in range(len(SelectedElements)):
        for c in range(nbCluster):
            ax_histo[1,i].plot(np.arange(-nbExpend,nbExpend),values_to_plot[0,c,:,i],lineshape[c],color="black",label="Cluster Center")
            ax_histo[1,i].scatter(np.arange(-nbExpend,nbExpend),values_to_plot[0,c,:,i],color="black",s=1)
            
            ax_histo[1,i].plot(np.arange(-nbExpend,nbExpend),values_to_plot[1,c,:,i],lineshape[c],color="red",label="Histogram Maximum")
            ax_histo[1,i].scatter(np.arange(-nbExpend,nbExpend),values_to_plot[1,c,:,i],color="red",s=1)
        
        ax_histo[1,i].set_title("Approximation composition "+SelectedElements[i])
        ax_histo[1,i].set_xlabel("Décalage du cluster")
        ax_histo[1,i].set_ylabel("Intensité")
        ax_histo[1,i].legend(loc="center right")
        ax_histo[1,i].set_ylim([0,None])
            
            
    fig, ax = plt.subplots(1)
    ax.imshow(ClusterSum,cmap="rainbow")

#Pour faire une PCA avec les résidus calculés
def Show_PCA_Compo_Spectra(array_3d,nbCompo,Show=True):
    X,Y,N=np.shape(array_3d)
    pca = PCA(n_components=nbCompo)

    PCA_fit=pca.fit(array_3d.reshape(X*Y,N))
    CompoVal=PCA_fit.components_
    PCA_mean_vector = PCA_fit.mean_
    
    PCA_Map=PCA_fit.transform(array_3d.reshape(X*Y,N))
    PCA_Map=PCA_Map.reshape(X,Y,nbCompo)

    
    if Show:
        
        fig,ax=plt.subplots(nbCompo,2)
        
        bar_width = 0.35  # Adjust the width of the bars
        x = np.arange(len(Elements_Names[1:]))
        
        for c in range(nbCompo):
            ax_map=ax[c, 0]
            ax_bar=ax[c, 1]
            
            ax_map.imshow(PCA_Map[:, :, c], cmap="inferno")
            ax_map.set_title(f'PCA Map {c}')
            
            ax_bar.bar(x + c * bar_width, CompoVal[c,:], width=bar_width,color="teal")
            ax_bar.set_xticks(x + bar_width * (nbCluster - 1) / 2, Elements_Names[1:])
            
            ax_map.set_title(f'PCA Spectra {c}')
        
    
    return PCA_Map,CompoVal,PCA_mean_vector    

#Represente 3 des compo de la PCA en carto RGB
def Show_PCA_RGB(PCA_Map,SelectedCompo,sign):
    Carto_RGB_Pos=np.zeros([nx,ny,3])
    Carto_RGB_Neg=np.zeros([nx,ny,3])
    
    for i in range(len(SelectedCompo)):
        x=(sign[i]*PCA_Map[:,:,SelectedCompo[i]]+abs(PCA_Map[:,:,SelectedCompo[i]]))/2
        x=np.log(1+x-np.min(x))
        Carto_RGB_Pos[:,:,i] = (x-np.min(x))/(np.max(x)-np.min(x))
        
        x=(-sign[i]*PCA_Map[:,:,SelectedCompo[i]]+abs(PCA_Map[:,:,SelectedCompo[i]]))/2
        x=np.log(1+x-np.min(x))
        Carto_RGB_Neg[:,:,i] = (x-np.min(x))/(np.max(x)-np.min(x))

    fig,ax=plt.subplots(2)
    
    color=["r","g","b"]
    for i in range(len(SelectedCompo)):
        ax[0].scatter(0,0,color=color[i],label="Compo "+str(SelectedCompo[i]))
        ax[1].scatter(0,0,color=color[i],label="Compo "+str(SelectedCompo[i]))

    ax[0].imshow(Carto_RGB_Pos)
    ax[1].imshow(Carto_RGB_Neg)
    
    ax[0].set_title("Résidu PCA Positif")
    ax[1].set_title("Résidu PCA Negatif")
    
    legend0=ax[0].legend(loc="lower right",frameon=False)
    for text in legend0.get_texts():
        text.set_color('white')
    
    legend1=ax[1].legend(loc="lower right",frameon=False)
    for text in legend1.get_texts():
        text.set_color('white')

#Pour trouver les pixel de la cartographie ou il y a le moins de Fer. J'ai écrit ça car c'est une autre méthode
#pour trouver la composition du fossile : faire la moyenne des spectre de chaque pixel ou il ya de moins de Fer
#(car pas de fer dans mon fossile) : cf fonction d'après
def generate_LeastFeMap(Fe_Map, MASK):
    if isinstance(MASK, np.ndarray):
        Fe_threshold = np.percentile(Fe_Map[MASK], 5)
        LeastFeMap = np.where((MASK == True) & (Fe_Map < Fe_threshold), 1, 0)
    else:
        Fe_threshold = np.percentile(Fe_Map, 5)
        LeastFeMap = np.where(Fe_Map < Fe_threshold, 1, 0)
    return LeastFeMap

#Fait la moyenne des pixels de la carto ou il ya le moins de Fer
def calculate_Sediment_Phase(Elements_Map, LeastFeMap):
    Sediment_Phase = np.zeros(nbElements)
    NX, NY, _ = Elements_Map.shape
    
    for e in range(nbElements):
        masked_elements_map = np.ma.masked_array(Elements_Map[:, :, e], LeastFeMap != 1)
        Sediment_Phase[e] = np.mean(masked_elements_map)
    
    return Sediment_Phase

Sample_Name="LAKPB2B9"

Elements_Map,Elements_Names,Ini_Compo,nx,ny,nbElements,MASK=LoadData(Sample_Name)

Plot_Elements(Elements_Map,Sample_Name)

ClusterMap,ClusterCenters,nbCluster=Kmean_Clustering(Elements_Map,Ini_Compo)
LeastFeMap = generate_LeastFeMap(Elements_Map[:,:,14], MASK)


#Expand_Cluster(ClusterMap,Elements_Map,["Si","P","Ca","Fe","K"],nbExpend=10)

Plot_Elements_and_Histo(Elements_Map,ClusterMap)
Pure_Phases=Calc_Pure_Phases(Elements_Map,ClusterMap)
Pure_Phases[:,0]=calculate_Sediment_Phase(Elements_Map, LeastFeMap)
#Pure_Phases=ClusterCenters.T

Composition = Estimate_Composition(Elements_Map, Pure_Phases,Show_Composition=True)

Reconstructed_Elements_Map = np.dot(Composition, Pure_Phases.T)
Difference = (Elements_Map - Reconstructed_Elements_Map)

Accuracy=np.linalg.norm(Difference,axis=2)

fig,ax=plt.subplots(1)
ax.imshow(1+Accuracy,cmap="inferno",norm=mpl.colors.LogNorm())
ax.set_title("Résidu")

Plot_Residus(Difference,Sample_Name,False)

#plt.scatter(Composition[:,:,0].reshape(-1),Difference[:,:,14].reshape(-1),s=1,alpha=0.05)

'''
SaveRGB=False

RGB_differences(["Fe","Si","K"],[-1,1,1],Save=SaveRGB)
RGB_differences(["Fe","Si","K"],[1,-1,-1],Save=SaveRGB)
RGB_differences(["Ca","P","S"],[-1,-1,-1],Save=SaveRGB)
RGB_differences(["Cr","Ti","Mn"],[1,1,1],Save=SaveRGB)
RGB_differences(["Si","P","Al"],[-1,1,1],Save=SaveRGB)
RGB_differences(["Si","P","Al"],[-1,-1,1],Save=SaveRGB)
RGB_differences(["Si","P","Al"],[1,1,1],Save=SaveRGB)
RGB_differences(["Si","P","Al"],[-1,-1,-1],Save=SaveRGB)
RGB_differences(["Si","P","Fe"],[-1,1,1],Save=SaveRGB)
RGB_differences(["Si","P","Fe"],[1,-1,-1],Save=SaveRGB)
RGB_differences(["K","Si"],[1,1],Save=SaveRGB)
RGB_differences(["K","Si"],[-1,-1],Save=SaveRGB)
RGB_differences(["P","Fe"],[1,1],Save=SaveRGB)
RGB_differences(["P","Fe"],[-1,-1],Save=SaveRGB)

PCA_Map,CompoVal,PCA_mean_vector=Show_PCA_Compo_Spectra(Difference[:,:,1:],10,Show=True)
Show_PCA_RGB(PCA_Map,[4,5,6],[1,1,1])

#Lets_Slide("P")

'''